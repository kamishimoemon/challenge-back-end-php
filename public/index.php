<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \GuzzleHttp\Client;

require(dirname(__DIR__) . '/vendor/autoload.php');

$ini = parse_ini_file(dirname(__DIR__) . '/config.ini', true);

$config = [
	'settings' => [
		'displayErrorDetails' => true,
		'addContentLengthHeader' => false,
		'spotify' => $ini['spotify'],
	],
];

$app = new \Slim\App($config);

$container = $app->getContainer();
$container['spotifyAccounts'] = function ($c) {
	$spotify = $c['settings']['spotify'];
	return new Client([
		'base_uri' => 'https://accounts.spotify.com',
		'auth' => [$spotify['clientId'], $spotify['clientSecret']],
		'verify' => dirname(__DIR__) . '/cacert.pem',
		'form_params' => [
			'grant_type' => 'client_credentials',
		],
	]);
};
$container['spotifyApi'] = function () {
	return new Client([
		'base_uri' => 'https://api.spotify.com/v1/',
		'verify' => dirname(__DIR__) . '/cacert.pem',
	]);
};

$app->get('/api/v1/albums', function (Request $request, Response $response) {
	$data = $request->getQueryParams();
	$bandName = filter_var($data['q'], FILTER_SANITIZE_STRING);

	$authResponse = $this->spotifyAccounts->post('/api/token');
	if (isErrorCode($authResponse->getStatusCode())) return $authResponse;
	$authBody = json_decode($authResponse->getBody()->getContents(), true);
	$accessToken = $authBody['access_token'];

	$searchResponse = $this->spotifyApi->request('GET', 'search', [
		'headers' => [
			'Authorization' => 'Bearer ' . $accessToken,
		],
		'query' => [
			'q' => $bandName,
			'type' => 'artist',
			'limit' => 1,
		],
	]);
	if (isErrorCode($searchResponse->getStatusCode())) return $searchResponse;
	$searchBody = json_decode($searchResponse->getBody()->getContents(), true);
	$artistId = $searchBody['artists']['items'][0]['id'];

	$albums = [];
	$albumBody['next'] = "artists/{$artistId}/albums?include_groups=album&limit=50";

	while (!empty($albumBody['next']))
	{
		$albumResponse = $this->spotifyApi->request('GET', $albumBody['next'], [
			'headers' => [
				'Authorization' => 'Bearer ' . $accessToken,
			]
		]);
		if (isErrorCode($albumResponse->getStatusCode()))
		{
			if (empty($albums)) return $albumResponse;
			else break;
		}
		$albumBody = json_decode($albumResponse->getBody()->getContents(), true);

		foreach ($albumBody['items'] as $album)
		{
			$albums[$album['name']] = [
				'name' => $album['name'],
				'released' => $album['release_date'],
				'tracks' => $album['total_tracks'],
				'cover' => [
					'height' => $album['images'][0]['height'],
					'width' => $album['images'][0]['width'],
					'url' => $album['images'][0]['url'],
				],
			];
		}
	}

	return $response->withJson(array_values($albums));
});
$app->run();

function isErrorCode ($statusCode) {
	return $statusCode < 200 || $statusCode >= 300;
}